/*
* Copyright (c) 2016, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define TAG "PlayerTest"

#include <fcntl.h>
#include <sys/mman.h>
#include <utils/Log.h>
#include <utils/String8.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <fstream>

#include "common/qmmf_common_utils.h"
#include "player/test/samples/qmmf_player_test.h"
#include "player/src/service/qmmf_player_common.h"


//#define DEBUG
#define TEST_INFO(fmt, args...)  ALOGD(fmt, ##args)
#define TEST_ERROR(fmt, args...) ALOGE(fmt, ##args)
#ifdef DEBUG
#define TEST_DBG  TEST_INFO
#else
#define TEST_DBG(...) ((void)0)
#endif

// Enable this define to dump audio bitstream from demuxer
//#define DUMP_AUDIO_BITSTREAM

// Enable this define to dump PCM from decoder
//#define DUMP_PCM_DATA

// Enable this define to dump video bitstream from demuxer
//#define DUMP_VIDEO_BITSTREAM

// Enable this define to dump YUV from decoder
//#define DUMP_YUV_FRAMES

// Disable this define for video only playback
#define AUDIO

// Disable this define for audio only playback
#define VIDEO

void PlayerTest::playercb(EventType event_type, void *event_data,
                          size_t event_data_size) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  Event* ev = (Event *)event_data;

  TEST_INFO("%s:%s Event type is %s", TAG, __func__,
      PlayerTestEvent[((int)event_type)]);

  TEST_INFO("%s:%s Player is in %s state", TAG,__func__,
      statemap_[(uint32_t)ev->state]);

  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

void PlayerTest::audiotrackcb(EventType event_type, void *event_data,
                              size_t event_data_size) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

void PlayerTest::videotrackcb(EventType event_type, void *event_data,
                              size_t event_data_size) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  TEST_INFO("%s:%s: Exit", TAG, __func__);
}


PlayerTest::PlayerTest()
    : stopped_(false), filename_(nullptr),
      release_parser_(false), start_again_(false),
      audioFirstFrame_(true), videoFirstFrame_(true),
      stop_playing_(false) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  if(filename_ != nullptr)
    m_pIStreamPort_ = new CMM_MediaSourcePort(filename_);

  PlayerTestEvent[0] = "Error";
  PlayerTestEvent[1] = "State Changed";

  statemap_.insert(std::pair<uint32_t, const char*> (0,"Error"));
  statemap_.insert(std::pair<uint32_t, const char*> (1,"Idle"));
  statemap_.insert(std::pair<uint32_t, const char*> (2,"Prepared"));
  statemap_.insert(std::pair<uint32_t, const char*> (4,"Started"));
  statemap_.insert(std::pair<uint32_t, const char*> (8,"Paused"));
  statemap_.insert(std::pair<uint32_t, const char*> (16,"Stopped"));
  statemap_.insert(std::pair<uint32_t, const char*> (32,"Playback Completed"));

  TEST_INFO("%s:%s: Exit", TAG, __func__);
}


PlayerTest::PlayerTest(char* filename_)
    : stopped_(false), filename_(nullptr),
      release_parser_(false), start_again_(false), audioFirstFrame_(true),
      videoFirstFrame_(true), stop_playing_(false) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  if (filename_ != nullptr)
    m_pIStreamPort_ = new CMM_MediaSourcePort(filename_);


  PlayerTestEvent[0] = "Error";
  PlayerTestEvent[1] = "State Changed";

  statemap_.insert(std::pair<uint32_t, const char *> (0,"Error"));
  statemap_.insert(std::pair<uint32_t, const char *> (1,"Idle"));
  statemap_.insert(std::pair<uint32_t, const char *> (2,"Prepared"));
  statemap_.insert(std::pair<uint32_t, const char *> (4,"Started"));
  statemap_.insert(std::pair<uint32_t, const char *> (8,"Paused"));
  statemap_.insert(std::pair<uint32_t, const char *> (16,"Stopped"));
  statemap_.insert(std::pair<uint32_t, const char *> (32,"Playback Completed"));

  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

PlayerTest::~PlayerTest() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  statemap_.clear();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

int32_t PlayerTest::Connect() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  PlayerCb player_cb_;

  player_cb_.event_cb = [&] (EventType event_type, void *event_data,
      size_t event_data_size) {playercb(event_type,event_data,
      event_data_size); };
  auto ret = player_.Connect(player_cb_);
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::Disconnect() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = player_.Disconnect();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::Prepare() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  if(m_pIStreamPort_ == nullptr && filename_ != nullptr)
    m_pIStreamPort_ = new CMM_MediaSourcePort(filename_);

  auto result = 0;

#ifdef DUMP_AUDIO_BITSTREAM
  srcFile_audio_.open("/data/dump_audio.bin", ios::binary | ios::out);
#endif

#ifdef DUMP_VIDEO_BITSTREAM
  srcFile_video_.open("/data/dump_video.bin", ios::binary | ios::out);
#endif

  AudioTrackCreateParam audio_track_param_;
  memset(&audio_track_param_, 0x0, sizeof audio_track_param_);
  fileCount_audio_ = 0;

  VideoTrackCreateParam video_track_param_;
  memset(&video_track_param_, 0x0, sizeof video_track_param_);
  fileCount_video_ = 0;

  ParseFile(audio_track_param_,video_track_param_);


#ifdef AUDIO
  TrackCb audio_track_cb_;

  audio_track_cb_.event_cb = [&] (EventType event_type, void *event_data,
      size_t event_data_size) {audiotrackcb(event_type, event_data,
      event_data_size);};

  result = player_.CreateAudioTrack(audio_track_id_,audio_track_param_,
      audio_track_cb_);
#endif


#ifdef VIDEO
 TrackCb video_track_cb_;

  video_track_cb_.event_cb = [&] (EventType event_type, void *event_data,
      size_t event_data_size) {videotrackcb(event_type, event_data,
      event_data_size);};

  result = player_.CreateVideoTrack(video_track_id_,video_track_param_,
      video_track_cb_);
#endif

  result = player_.Prepare();

  start_again_ = false;

  if (result != NO_ERROR)
    return -1;

  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return result;
}

int32_t PlayerTest::ParseFile(AudioTrackCreateParam& audio_track_param_,
                              VideoTrackCreateParam& video_track_param_) {
  TEST_INFO("%s:%s: Enter", TAG, __func__);

  auto result = 0;

  CreateDataSource();

#ifdef AUDIO
  audio_track_param_.sample_rate = m_sTrackInfo_.sAudio.ulSampleRate;
  audio_track_param_.channels    = m_sTrackInfo_.sAudio.ulChCount;
  audio_track_param_.bit_depth   = 16; //TODO m_sTrackInfo_.sAudio.ulBitDepth;
  audio_track_param_.codec       = (AudioCodecType)AudioFormat::kAAC;
  audio_track_param_.codec_params.aac.bit_rate = m_sTrackInfo_.sAudio.ulBitRate;
  audio_track_param_.codec_params.aac.format   = AACFormat::kRaw;
  audio_track_param_.codec_params.aac.mode     = AACMode::kAALC;
  audio_track_param_.out_device                = AudioOutSubtype::kBuiltIn;

  TEST_INFO("%s:%s audio_track_id_ : %d ", TAG, __func__, audio_track_id_);

  TEST_INFO("%s:%s sample rate : %d channel %d bitdepth %d, bitrate %d ", TAG
      , __func__, audio_track_param_.sample_rate, audio_track_param_.channels,
      audio_track_param_.bit_depth, m_sTrackInfo_.sAudio.ulBitRate);
#endif

#ifdef VIDEO
  video_track_param_.codec       = VideoCodecType::kAVC;
  video_track_param_.frame_rate  = m_sTrackInfo_.sVideo.fFrameRate;
  video_track_param_.height      = 1088; //TODO m_sTrackInfo_.sVideo.ulHeight;
  video_track_param_.width       = 1920; //TODO m_sTrackInfo_.sVideo.ulWidth;
  video_track_param_.bitrate     = m_sTrackInfo_.sVideo.ulBitRate;
  video_track_param_.num_buffers = 1;
  video_track_param_.out_device  = VideoOutSubtype::kHDMI;

  TEST_INFO("%s:%s video_track_id_ : %d ", TAG, __func__, video_track_id_);

  TEST_INFO("%s:%s height : %d width %d frame_rate %d, bitrate %d ", TAG
      , __func__, video_track_param_.height, video_track_param_.width,
      video_track_param_.frame_rate, video_track_param_.bitrate);
#endif

  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

int32_t PlayerTest::Start() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = 0;

  if(start_again_)
  {
    AudioTrackCreateParam audio_track_param_;
    memset(&audio_track_param_, 0x0, sizeof audio_track_param_);

    VideoTrackCreateParam video_track_param_;
    memset(&video_track_param_, 0x0, sizeof video_track_param_);

    ParseFile(audio_track_param_,video_track_param_);
  }

  ret = player_.Start();
  stopped_ = false;
  stop_playing_ = false;

#ifdef AUDIO
  pthread_create(&audio_thread_id_, nullptr, PlayerTest::StartPlayingAudio,
      (void*)this);
#endif

#ifdef VIDEO
  pthread_create(&video_thread_id_, nullptr, PlayerTest::StartPlayingVideo,
      (void*)this);
#endif

  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

void * PlayerTest::StartPlayingAudio(void *ptr) {
  TEST_INFO("%s:%s: Enter", TAG, __func__);

  auto ret = 0;

  uint32_t result;

  PlayerTest* playertest = static_cast<PlayerTest *>(ptr);
  std::vector<TrackBuffer> buffers;
  TrackBuffer tb;

  while (!playertest->stopped_)
  {
    memset(&tb,0x0,sizeof(tb));
    buffers.push_back(tb);
    uint32_t val = 1;

    ret = playertest->player_.DequeueInputBuffer(playertest->audio_track_id_,
        buffers);

    int32_t num_frames_read;
    uint32_t bytes_read;

    FileSourceSampleInfo sSampleInfo;
    FileSourceMediaStatus eMediaStatus = FILE_SOURCE_DATA_ERROR;
    memset(&sSampleInfo, 0, sizeof(FileSourceSampleInfo));

    playertest->m_sTrackInfo_.sAudio.sSampleBuf.pucData1 =
        static_cast<uint8*>(buffers[0].data);

    playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulLen = buffers[0].size;

    playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulLen =
        playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulMaxLen;

    uint32_t nFormatBlockSize =0;

    if (playertest->audioFirstFrame_) {
      uint32_t status = playertest->m_pDemux_->m_pFileSource->GetFormatBlock(
          playertest->m_sTrackInfo_.sAudio.ulTkId, nullptr, &nFormatBlockSize);
      TEST_DBG("%s:%s: Audio getFormatBlock size = %lu", TAG, __func__,
          nFormatBlockSize);

      uint8_t *buffer = new uint8_t[nFormatBlockSize];
      if (buffer != nullptr) {
        status = playertest->m_pDemux_->m_pFileSource->GetFormatBlock(
            playertest->m_sTrackInfo_.sAudio.ulTkId, buffer, &nFormatBlockSize);
      }

      memcpy(buffers[0].data , buffer, nFormatBlockSize );
      delete buffer;
      playertest->audioFirstFrame_ = false;
    }

    eMediaStatus = playertest->m_pDemux_->GetNextMediaSample(
        playertest->m_sTrackInfo_.sAudio.ulTkId,
        playertest->m_sTrackInfo_.sAudio.sSampleBuf.pucData1+nFormatBlockSize,
        &(playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulLen), sSampleInfo);

#ifdef DUMP_AUDIO_BITSTREAM
    playertest->srcFile_audio_.write((const char*) buffers[0].data,
        playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulLen);
    playertest->fileCount_audio_++;
#endif

    buffers[0].filled_size = playertest->m_sTrackInfo_.sAudio.sSampleBuf.ulLen +
        nFormatBlockSize ;

    if (FILE_SOURCE_DATA_END == eMediaStatus || playertest->stopped_) {
      //EOF reached or Stopped
      TEST_INFO("%s:%s:File read completed result is %d", TAG, __func__, result);
      buffers[0].flag = 1;
      buffers[0].filled_size = 0;

      TEST_DBG("%s:%s: audio_filled_size %d", TAG, __func__,
          buffers[0].filled_size);
      TEST_DBG("%s:%s: audio_buffer size %d", TAG, __func__, buffers[0].size);
      TEST_DBG("%s:%s: audio_vaddr 0x%x", TAG, __func__, buffers[0].data);

      playertest->player_.QueueInputBuffer(playertest->audio_track_id_, buffers,
          (void*)&val, sizeof (uint32_t), TrackMetaBufferType::kNone);
      buffers.clear();
      playertest->stopped_ = true;
      if (!playertest->stop_playing_)
        playertest->StopPlaying();
      break;
    }

    TEST_DBG("%s:%s: audio_filled_size %d", TAG, __func__,
        buffers[0].filled_size);
    TEST_DBG("%s:%s: audio_buffer size %d", TAG, __func__, buffers[0].size);
    TEST_DBG("%s:%s: audio_vaddr 0x%x", TAG, __func__, buffers[0].data);

    playertest->player_.QueueInputBuffer(playertest->audio_track_id_, buffers,
        (void*)&val, sizeof (uint32_t), TrackMetaBufferType::kNone);
    buffers.clear();
  }

  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return NULL;
}

void * PlayerTest::StartPlayingVideo(void *ptr) {
  TEST_INFO("%s:%s: Enter", TAG, __func__);

  auto ret = 0;

  uint32_t result;

  PlayerTest* playertest = static_cast<PlayerTest *>(ptr);
  std::vector<TrackBuffer> buffers;
  TrackBuffer tb;

  while (!playertest->stopped_)
  {
    memset(&tb,0x0,sizeof(tb));
    buffers.push_back(tb);
    uint32_t val = 1;

    ret = playertest->player_.DequeueInputBuffer(playertest->video_track_id_,
        buffers);

    //Video
    int32_t num_frames_read;
    uint32_t bytes_read;

    FileSourceSampleInfo sSampleInfo;
    FileSourceMediaStatus eMediaStatus = FILE_SOURCE_DATA_ERROR;
    memset(&sSampleInfo, 0, sizeof(FileSourceSampleInfo));

    playertest->m_sTrackInfo_.sVideo.sSampleBuf.pucData1 =
        static_cast<uint8*>(buffers[0].data);

    playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulLen = buffers[0].size;

    playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulLen =
        playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulMaxLen;

    uint32_t nFormatBlockSize =0;

    if (playertest->videoFirstFrame_) {
      uint32_t status = playertest->m_pDemux_->m_pFileSource->GetFormatBlock(
          playertest->m_sTrackInfo_.sVideo.ulTkId, nullptr, &nFormatBlockSize);
      TEST_DBG("%s:%s: Video getFormatBlock size = %lu", TAG, __func__,
          nFormatBlockSize);

      uint8_t *buffer = new uint8_t[nFormatBlockSize];
      if (buffer != nullptr) {
        status = playertest->m_pDemux_->m_pFileSource->GetFormatBlock(
            playertest->m_sTrackInfo_.sVideo.ulTkId, buffer, &nFormatBlockSize);
      }

      memcpy(buffers[0].data , buffer, nFormatBlockSize );
      delete buffer;
      playertest->videoFirstFrame_ = false;
    }

    eMediaStatus = playertest->m_pDemux_->GetNextMediaSample(
        playertest->m_sTrackInfo_.sVideo.ulTkId,
        playertest->m_sTrackInfo_.sVideo.sSampleBuf.pucData1 + nFormatBlockSize,
        &(playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulLen), sSampleInfo);

#ifdef DUMP_VIDEO_BITSTREAM
    playertest->srcFile_video_.write((const char*) buffers[0].data,
        playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulLen);
    playertest->fileCount_video_++;
#endif

    buffers[0].filled_size = playertest->m_sTrackInfo_.sVideo.sSampleBuf.ulLen +
        nFormatBlockSize ;

    if (FILE_SOURCE_DATA_END == eMediaStatus || playertest->stopped_) {
      //EOF reached or Stopped
      TEST_INFO("%s:%s:File read completed result is %d", TAG, __func__, result);
      buffers[0].flag = 1;
      buffers[0].filled_size = 0;

      TEST_DBG("%s:%s: video_filled_size %d", TAG, __func__,
          buffers[0].filled_size);
      TEST_DBG("%s:%s: video_buffer size %d", TAG, __func__, buffers[0].size);
      TEST_DBG("%s:%s: video_vaddr 0x%x", TAG, __func__, buffers[0].data);

      playertest->player_.QueueInputBuffer(playertest->video_track_id_, buffers,
          (void*)&val, sizeof (uint32_t), TrackMetaBufferType::kNone);
      buffers.clear();
      playertest->stopped_ = true;
      if (!playertest->stop_playing_)
        playertest->StopPlaying();
      break;
    }

    TEST_DBG("%s:%s: video_filled_size %d", TAG, __func__,
        buffers[0].filled_size);
    TEST_DBG("%s:%s: video_buffer size %d", TAG, __func__, buffers[0].size);
    TEST_DBG("%s:%s: video_vaddr 0x%x", TAG, __func__, buffers[0].data);

    playertest->player_.QueueInputBuffer(playertest->video_track_id_, buffers,
        (void*)&val, sizeof (uint32_t), TrackMetaBufferType::kNone);
    buffers.clear();
  }

  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return NULL;
}

int32_t PlayerTest::Stop() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  stopped_ = true;
  TEST_INFO("%s:%s: Exit", TAG, __func__);
}

int32_t PlayerTest::StopPlaying() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = -1;

  stop_playing_ = true;

#if (defined (AUDIO) && defined (VIDEO))
   {
    pthread_join(audio_thread_id_, NULL);
    pthread_join(video_thread_id_, NULL);
   }
#endif

  ret = player_.Stop(false);

#ifdef DUMP_AUDIO_BITSTREAM
  if(srcFile_audio_.is_open())
    srcFile_audio_.close();
#endif

#ifdef DUMP_VIDEO_BITSTREAM
  if(srcFile_video_.is_open())
    srcFile_video_.close();
#endif

  start_again_ = true;
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::Pause() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = player_.Pause();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::Resume() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = player_.Resume();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::SetPosition() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
    auto ret = 0;
  //int64_t seek_time;
  //auto ret = player_.SetPosition(seek_time);
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::SetTrickMode() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = 0;
  //auto ret = player_.SetTrickMode();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::GrabPicture() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
    auto ret = 0;
  //player_.GrabPicture();
  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

int32_t PlayerTest::Delete() {
  TEST_INFO("%s:%s: Enter", TAG, __func__);
  auto ret = 0;

#ifdef AUDIO
  player_.DeleteAudioTrack(audio_track_id_);
#endif

#ifdef VIDEO
  player_.DeleteVideoTrack(video_track_id_);
#endif

  delete m_pIStreamPort_;
  m_pIStreamPort_ = NULL;
  audioFirstFrame_ = true;
  videoFirstFrame_ = true;

  TEST_INFO("%s:%s: Exit", TAG, __func__);
  return ret;
}

uint32_t PlayerTest::CreateDataSource() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);
  uint32_t eErr = MM_STATUS_ErrorNone;

  m_pDemux_ = CMM_MediaDemuxInt::New(*m_pIStreamPort_, FILE_SOURCE_MPEG4);

  if (!m_pDemux_) {
    TEST_ERROR("%s %s DataSource CreationFAILURE!!", TAG, __func__);
    BAIL_ON_ERROR(MM_STATUS_ErrorDefault);
  }

  TEST_INFO("%s:%s: DataSource Creation SUCCESS!!", TAG, __func__);

  //Read file meta-data
  eErr = ReadMediaInfo();
  BAIL_ON_ERROR(eErr);

  TEST_INFO("%s:%s: Exit", TAG, __func__);

ERROR_BAIL:
  return eErr;
}

uint32_t PlayerTest::ReadMediaInfo() {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  uint32_t eErr = 0;
  uint32 ulNumTracks = 0;
  FileSourceTrackIdInfoType aTrackList[MM_SOURCE_MAX_TRACKS];
  FileSourceMjMediaType eMjType = FILE_SOURCE_MJ_TYPE_UNKNOWN;
  FileSourceMnMediaType eMnType = FILE_SOURCE_MN_TYPE_UNKNOWN;
  FileSourceStatus eFS_Status = FILE_SOURCE_FAIL;

  // Get total number of tracks available.
  m_sTrackInfo_.ulNumTracks = m_pDemux_->GetWholeTracksIDList(aTrackList);
  TEST_INFO("%s:%s: NumTracks = %u", TAG, __func__, ulNumTracks);

  for (uint32 ulIdx = 0; ulIdx < m_sTrackInfo_.ulNumTracks; ulIdx++) {
    FileSourceTrackIdInfoType sTrackInfo = aTrackList[ulIdx];

    // Get MimeType
    eFS_Status = m_pDemux_->GetMimeType(sTrackInfo.id, eMjType, eMnType);
    if(FILE_SOURCE_SUCCESS != eFS_Status) {
      TEST_INFO("%s:%s: Unable to get MIME_TYPE = %u", TAG, __func__,
          eFS_Status);
      continue;
    }

    if (FILE_SOURCE_SUCCESS == eFS_Status ) {
      if ( FILE_SOURCE_MJ_TYPE_AUDIO == eMjType ) {
        TEST_INFO("%s:%s: TRACK_AUDIO @MIME_TYPE = %u", TAG, __func__,
          eMnType);
        m_sTrackInfo_.sAudio.bTrackSelected = sTrackInfo.selected;

       TEST_INFO("%s : %s id:%d ", __func__, TAG, sTrackInfo.id);

        eErr = ReadAudioTrackMediaInfo(sTrackInfo.id, eMnType);
        audio_track_id_ = sTrackInfo.id;

      } else if (FILE_SOURCE_MJ_TYPE_VIDEO == eMjType) {
        TEST_INFO("%s:%s: TRACK_VIDEO @MIME_TYPE = %u", TAG, __func__,
          eMnType);

        m_sTrackInfo_.sVideo.bTrackSelected = sTrackInfo.selected;

        TEST_INFO("%s : %s id:%d ", __func__, TAG, sTrackInfo.id);

        eErr = ReadVideoTrackMediaInfo(sTrackInfo.id, eMnType);
        video_track_id_ = sTrackInfo.id;
      }
    }
    else {
      eErr = MM_STATUS_ErrorStreamCorrupt;
      TEST_ERROR("%s %sFailed to identify Tracks Error= %u", TAG, __func__,
      eFS_Status);
      BAIL_ON_ERROR(eErr);
    }
  }

  TEST_INFO("%s:%s: Exit", TAG, __func__);

ERROR_BAIL:
  return eErr;
}

uint32_t PlayerTest::ReadAudioTrackMediaInfo(
    uint32 ulTkId,
    FileSourceMnMediaType eTkMnType) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  MM_STATUS_TYPE eErr = MM_STATUS_ErrorNone;
  FileSourceStatus eFS_Status = FILE_SOURCE_FAIL;
  MediaTrackInfo sMediaInfo;
  memset(&sMediaInfo, 0, sizeof(MediaTrackInfo));

  // Get max buffer size
  m_sTrackInfo_.sAudio.sSampleBuf.ulMaxLen = \
    m_pDemux_->GetTrackMaxFrameBufferSize(ulTkId);

  // Get track media information
  eFS_Status = m_pDemux_->GetMediaTrackInfo(ulTkId, &sMediaInfo);

  // Update track media information
  if (FILE_SOURCE_SUCCESS == eFS_Status) {
    m_sTrackInfo_.sAudio.ulTkId = ulTkId;
    m_sTrackInfo_.sAudio.ulCodecType = eTkMnType;
    m_sTrackInfo_.sAudio.ulChCount = \
                                sMediaInfo.audioTrackInfo.numChannels;
    m_sTrackInfo_.sAudio.ulBitRate = \
                                sMediaInfo.audioTrackInfo.bitRate;
    m_sTrackInfo_.sAudio.ulSampleRate = \
                                sMediaInfo.audioTrackInfo.samplingRate;
    m_sTrackInfo_.sAudio.ulBitDepth = \
                                sMediaInfo.audioTrackInfo.nBitsPerSample;
    m_sTrackInfo_.sAudio.ullDuration = \
                                sMediaInfo.audioTrackInfo.duration;
    m_sTrackInfo_.sAudio.ulTimeScale = \
                                sMediaInfo.audioTrackInfo.timeScale;

    TEST_INFO("%s:%s: TkId = %u CH= %u  SR= %u BD=%u", TAG, __func__, ulTkId,
        m_sTrackInfo_.sAudio.ulChCount, m_sTrackInfo_.sAudio.ulSampleRate,
        m_sTrackInfo_.sAudio.ulBitDepth);

    // Get track CSD data len
    eFS_Status = m_pDemux_->GetFormatBlock(ulTkId,
                                          nullptr,
                                          &m_sTrackInfo_.sAudio.sCSD.ulLen,
                                          FALSE);
    BAIL_ON_ERROR(eFS_Status);

    // Get track CSD data if CSD len is valid
    if (0 != m_sTrackInfo_.sAudio.sCSD.ulLen) {
      TEST_INFO("%s:%s: CSD Len = %u", TAG, __func__,
        m_sTrackInfo_.sAudio.sCSD.ulLen);

      m_sTrackInfo_.sAudio.sCSD.pucData = \
        (uint8*)MM_Malloc(sizeof(uint8)* m_sTrackInfo_.sAudio.sCSD.ulLen);
      if (!m_sTrackInfo_.sAudio.sCSD.pucData) {
        eErr = MM_STATUS_ErrorMemAllocFail;
        BAIL_ON_ERROR(eErr);
      }
      eFS_Status = m_pDemux_->GetFormatBlock(ulTkId,
                                            m_sTrackInfo_.sAudio.sCSD.pucData,
                                            &m_sTrackInfo_.sAudio.sCSD.ulLen,
                                            FALSE);
      BAIL_ON_ERROR(eFS_Status);
    }
  }

  TEST_INFO("%s:%s: Exit", TAG, __func__);

ERROR_BAIL:
  if (FILE_SOURCE_SUCCESS != eFS_Status) {
    eErr = MM_STATUS_ErrorDefault;
  }
  TEST_ERROR("%s:%s: Return Status %u", TAG, __func__, eErr);
  return eErr;
}

uint32_t PlayerTest::ReadVideoTrackMediaInfo(
    uint32 ulTkId,
    FileSourceMnMediaType eTkMnType) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  MM_STATUS_TYPE eErr = MM_STATUS_ErrorNone;
  FileSourceStatus eFS_Status = FILE_SOURCE_FAIL;
  MediaTrackInfo sMediaInfo;
  memset(&sMediaInfo, 0, sizeof(MediaTrackInfo));

  m_sTrackInfo_.sVideo.sSampleBuf.ulMaxLen = \
    m_pDemux_->GetTrackMaxFrameBufferSize(ulTkId);
  eFS_Status = m_pDemux_->GetMediaTrackInfo(ulTkId, &sMediaInfo);
  if(FILE_SOURCE_SUCCESS == eFS_Status){
    m_sTrackInfo_.sVideo.ulTkId = ulTkId;
    m_sTrackInfo_.sVideo.ulCodecType = sMediaInfo.videoTrackInfo.videoCodec;
    m_sTrackInfo_.sVideo.ulWidth = sMediaInfo.videoTrackInfo.frameWidth;
    m_sTrackInfo_.sVideo.ulHeight = sMediaInfo.videoTrackInfo.frameHeight;
    m_sTrackInfo_.sVideo.fFrameRate = sMediaInfo.videoTrackInfo.frameRate;
    m_sTrackInfo_.sVideo.ulBitRate = sMediaInfo.videoTrackInfo.bitRate;
    m_sTrackInfo_.sVideo.ullDuration = sMediaInfo.videoTrackInfo.duration;
    m_sTrackInfo_.sVideo.ulTimeScale = sMediaInfo.videoTrackInfo.timeScale;

    TEST_INFO("%s:%s: TkId = %u Width= %u  Height= %u FR=%f", TAG, __func__,
        ulTkId, m_sTrackInfo_.sVideo.ulWidth, m_sTrackInfo_.sVideo.ulHeight,
        m_sTrackInfo_.sVideo.fFrameRate);

    // Get CSD data len
    eFS_Status = m_pDemux_->GetFormatBlock(ulTkId,
                                          nullptr,
                                          &m_sTrackInfo_.sVideo.sCSD.ulLen,
                                          FALSE);
    BAIL_ON_ERROR(eFS_Status);
    if (0 != m_sTrackInfo_.sVideo.sCSD.ulLen) {
      TEST_INFO("%s:%s: CSD Len = %u", TAG, __func__,
        m_sTrackInfo_.sVideo.sCSD.ulLen);

      m_sTrackInfo_.sVideo.sCSD.pucData = \
              (uint8*)MM_Malloc(sizeof(uint8)* m_sTrackInfo_.sVideo.sCSD.ulLen);
      if (!m_sTrackInfo_.sVideo.sCSD.pucData) {
        eErr = MM_STATUS_ErrorMemAllocFail;
        TEST_ERROR("%s %s CSD Alloc failure", TAG, __func__);
        BAIL_ON_ERROR(eErr);
      }
      eFS_Status = m_pDemux_->GetFormatBlock(ulTkId,
                                            m_sTrackInfo_.sVideo.sCSD.pucData,
                                            &m_sTrackInfo_.sVideo.sCSD.ulLen,
                                            FALSE);
      BAIL_ON_ERROR(eFS_Status);
    }
  }

  TEST_INFO("%s:%s: Exit", TAG, __func__);

ERROR_BAIL:
  if(FILE_SOURCE_SUCCESS != eFS_Status){
    eErr = MM_STATUS_ErrorDefault;
  }

  TEST_ERROR("%s:%s: Return Status %u", TAG, __func__, eErr);
  return eErr;
}

void CmdMenu::PrintMenu() {

  printf("\n\n=========== PLAYER TEST MENU ===================\n\n");

  printf(" \n\nPlayer Test Application commands \n");
  printf(" -----------------------------\n");
  printf("   %c. Connect\n", CmdMenu::CONNECT_CMD);
  printf("   %c. Disconnect\n", CmdMenu::DISCONNECT_CMD);
  printf("   %c. Prepare\n", CmdMenu::PREPARE_CMD);
  printf("   %c. Start\n", CmdMenu::START_CMD);
  printf("   %c. Stop\n", CmdMenu::STOP_CMD);
  printf("   %c. Pause\n", CmdMenu::PAUSE_CMD);
  printf("   %c. Resume\n", CmdMenu::RESUME_CMD);
  printf("   %c. Delete\n", CmdMenu::DELETE_CMD);
  printf("   %c. Exit\n", CmdMenu::EXIT_CMD);
  printf("\n   Choice: ");
}

CmdMenu::Command CmdMenu::GetCommand() {
  PrintMenu();
  return CmdMenu::Command(static_cast<CmdMenu::CommandType>(getchar()));
}

int main(int argc,char *argv[]) {

  TEST_INFO("%s:%s: Enter", TAG, __func__);

  PlayerTest test_context(argv[1]);

  CmdMenu cmd_menu(test_context);

  int32_t exit_test = false;

  if (argc == 2) {
    char *extn = strrchr(argv[1], '.');
    TEST_INFO("%s: exten is: %s", TAG, extn);

  } else {
      TEST_INFO("%s:%s Give some file to play", TAG,__func__);
      exit_test = true;
  }

  while (!exit_test) {

    CmdMenu::Command command = cmd_menu.GetCommand();
    switch (command.cmd) {

      case CmdMenu::CONNECT_CMD: {
        test_context.Connect();
      }
      break;
      case CmdMenu::DISCONNECT_CMD: {
        test_context.Disconnect();
      }
      break;
      case CmdMenu::PREPARE_CMD: {
        test_context.Prepare();
      }
      break;
      case CmdMenu::START_CMD: {
        test_context.Start();
      }
      break;
      case CmdMenu::STOP_CMD: {
        test_context.Stop();
      }
      break;
      case CmdMenu::PAUSE_CMD: {
        test_context.Pause();
      }
      break;
      case CmdMenu::RESUME_CMD: {
        test_context.Resume();
      }
      break;
      case CmdMenu::DELETE_CMD: {
        test_context.Delete();
      }
      break;
      case CmdMenu::EXIT_CMD: {
        exit_test = true;
      }
      break;
      default:
        break;
    }
  }
  return 0;
}
